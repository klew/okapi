/*===========================================================================
  Copyright (C) 2018 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.markdown;

import static net.sf.okapi.common.filters.FilterTestUtil.assertDocumentPart;
import static net.sf.okapi.common.filters.FilterTestUtil.assertTUListContains;
import static net.sf.okapi.common.filters.FilterTestUtil.assertTUListDoesNotContain;
import static net.sf.okapi.common.filters.FilterTestUtil.assertTextUnit;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Ignore;
import org.junit.Test;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.TextFragment;

public class MarkdownFilterTest {

    /*
     * This returns a UPA character pair representing n'th isolated place holder.
     */
    private final static String IPH(int n) {
        return new String(new char[]{TextFragment.MARKER_ISOLATED, (char)(TextFragment.CHARBASE+n)});
    }
    private final static String IPH0 = IPH(0);
    private final static String IPH1 = IPH(1);
    private final static String IPH2 = IPH(2);


    @Test
    public void testCloseWithoutInput() throws Exception {
        MarkdownFilter filter = new MarkdownFilter();
        filter.close();
    }

    @Test
    public void testEventsFromEmptyInput() {
        String snippet = "";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 2, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            assertEquals(EventType.END_DOCUMENT, events.get(1).getEventType());
        }
    }

    @Test
    public void testAutoLink() {
        String snippet = "<https://www.google.com>";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 5, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            assertDocumentPart(events.get(1), "<https://www.google.com>");
            assertDocumentPart(events.get(2), System.lineSeparator());
            assertDocumentPart(events.get(3), System.lineSeparator());
            assertEquals(EventType.END_DOCUMENT, events.get(4).getEventType());
        }
    }

    @Test
    public void testBlockQuoteEvents() {
        String snippet = "> Blockquote";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 6, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            assertDocumentPart(events.get(1), "> ");
            assertTextUnit(events.get(2), "Blockquote");
            assertDocumentPart(events.get(3), System.lineSeparator());
            assertDocumentPart(events.get(4), System.lineSeparator());
            assertEquals(EventType.END_DOCUMENT, events.get(5).getEventType());
        }
    }

    @Test
    public void testBulletList() {
        String snippet = "* First\nelement\n\n" + "* Second element\n\n" + "* Third element\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 14, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());

            assertDocumentPart(events.get(1), "* ");
            assertTextUnit(events.get(2), "First\nelement");
            assertDocumentPart(events.get(3), "\n");
            assertDocumentPart(events.get(4), "\n");

            assertDocumentPart(events.get(5), "* ");
            assertTextUnit(events.get(6), "Second element");
            assertDocumentPart(events.get(7), "\n");
            assertDocumentPart(events.get(8), "\n");

            assertDocumentPart(events.get(9), "* ");
            assertTextUnit(events.get(10), "Third element");
            assertDocumentPart(events.get(11), "\n");
            assertDocumentPart(events.get(12), "\n");

            assertEquals(EventType.END_DOCUMENT, events.get(13).getEventType());
        }
    }

    @Test
    public void testCode() {
        String snippet = "`Text`";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 6, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            assertDocumentPart(events.get(1), "`");
            assertTextUnit(events.get(2), "Text`", "`");
            assertDocumentPart(events.get(3), System.lineSeparator());
            assertDocumentPart(events.get(4), System.lineSeparator());
            assertEquals(EventType.END_DOCUMENT, events.get(5).getEventType());
        }
    }

    @Test
    public void testEmphasis() {
        String snippet = "_Text_";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 6, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            assertDocumentPart(events.get(1), "_");
            assertTextUnit(events.get(2), "Text_", "_");
            assertDocumentPart(events.get(3), System.lineSeparator());
            assertDocumentPart(events.get(4), System.lineSeparator());
            assertEquals(EventType.END_DOCUMENT, events.get(5).getEventType());
        }
    }

    @Test
    public void testFencedCodeBlock() {
        String snippet = "```{java}\n"
                + "Content in a fenced code block\n"
                + "```\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 10, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());

            assertDocumentPart(events.get(1), "```");
            assertDocumentPart(events.get(2), "{java}");
            assertDocumentPart(events.get(3), "\n");
            assertTextUnit(events.get(4), "Content in a fenced code block");
            assertDocumentPart(events.get(5), "\n");
            assertDocumentPart(events.get(6), "```");
            assertDocumentPart(events.get(7), "\n");
            assertDocumentPart(events.get(8), "\n");

            assertEquals(EventType.END_DOCUMENT, events.get(9).getEventType());
        }
    }

    @Test
    public void testHeadingPrefix() {
        String snippet = "# Heading";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 6, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            assertDocumentPart(events.get(1), "# ");
            assertTextUnit(events.get(2), "Heading");
            assertDocumentPart(events.get(3), System.lineSeparator());
            assertDocumentPart(events.get(4), System.lineSeparator());
            assertEquals(EventType.END_DOCUMENT, events.get(5).getEventType());
        }
    }

    @Test
    public void testHeadingPrefixWithoutSpace() {
        // Note: This was supported by Github Markdown but is no longer supported.
        // The tokens are of type TEXT but are meant to be headers, so have the filter convert the '#'s to code
        String snippet = "#Heading 1\n\n" + "##Heading 2\n\n" + "###Heading 3";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);

            assertEquals("#events", 14, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());

            assertDocumentPart(events.get(1), "# ");
            assertTextUnit(events.get(2), "Heading 1");

            assertDocumentPart(events.get(3), "\n");
            assertDocumentPart(events.get(4), "\n");

            assertDocumentPart(events.get(5), "## ");
            assertTextUnit(events.get(6), "Heading 2");
            assertDocumentPart(events.get(7), "\n");
            assertDocumentPart(events.get(8), "\n");

            assertDocumentPart(events.get(9), "### ");
            assertTextUnit(events.get(10), "Heading 3");
            assertDocumentPart(events.get(11), "\n");
            assertDocumentPart(events.get(12), "\n");

            assertEquals(EventType.END_DOCUMENT, events.get(13).getEventType());
        }
    }

    @Test
    public void testHeadingUnderline() {
        String snippet = "Heading\n=======\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 7, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());

            assertTextUnit(events.get(1), "Heading");
            assertDocumentPart(events.get(2), "\n");
            assertDocumentPart(events.get(3), "=======");
            assertDocumentPart(events.get(4), "\n");
            assertDocumentPart(events.get(5), "\n");

            assertEquals(EventType.END_DOCUMENT, events.get(6).getEventType());
        }
    }

    @Test
    public void testHtmlTable() {
        String snippet = "<table><tr><td>Test</td></tr></table>\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);

            assertEquals("#events", 12, events.size());
            assertTextUnit(events.get(4), "Test"); // <td>Test</td> where <td> and </td> are in the skeleton, not in the codes.
        }
    }

    @Test
    public void testHtmlBlockWithMarkdown() {
        String snippet = "<table><tr><td>**Bold** *Italic*</td></tr></table>\n";;
        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertTUListContains(tus, "**Bold** *Italic*");
            assertTUListDoesNotContain(tus, "table");
        }
    }

    @Test
    public void testHtmlInline() {
        String snippet = "This contains <span>some inline</span> HTML\n\n";
        // Flexmark does not recognize the run of text <span>some inline</span> as one unit.
        // It only recognizes each of <span> and </span> as an HTML Inline element.

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertTUListContains(tus, "This contains");
            assertTUListContains(tus, "some inline");
            assertTUListContains(tus, "HTML");
            assertTUListDoesNotContain(tus, "span");
        }
    }

    @Test
    public void testHtmlInlineWithAttributes() {
        String snippet = "Sentence 1. <span class=\"foo\">Sentence 2.</span> Sentence 3.";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            List<ITextUnit> tus =
                    FilterTestDriver.filterTextUnits(FilterTestDriver.getEvents(filter, snippet, null));
            assertEquals(5, tus.size());
            assertTextUnit(tus.get(0), "Sentence 1. ");
            assertTextUnit(tus.get(1), "<span class=\"foo\">", "<span class=\"foo\">");
            assertTextUnit(tus.get(2), "Sentence 2.");
            assertTextUnit(tus.get(3), "</span>", "</span>");
            assertTextUnit(tus.get(4), " Sentence 3.");
        }
    }

    @Test
    public void testHtmlBreakElement() {
        String snippet = "<p>Sentence 1.<br/>Sentence 2.</p>";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            List<ITextUnit> tus =
                    FilterTestDriver.filterTextUnits(FilterTestDriver.getEvents(filter, snippet, null));
            assertEquals(1, tus.size());
            assertTextUnit(tus.get(0), "Sentence 1.<br/>Sentence 2.", "<br/>");
        }
    }

    @Test
    public void testHtmlCommentAtColumn1() {
        String snippet = "<!-- this line has only an HTML comment that won't be rendered. So it should be untranslatable. -->";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            List<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus =
                    FilterTestDriver.filterTextUnits(FilterTestDriver.getEvents(filter, snippet, null));
            assertTUListDoesNotContain(tus, "this");
            assertTUListDoesNotContain(tus, "untranslatable");

        }
    }

    @Test
    public void testHtmlCommentAtColumn5() {
        String snippet = "        <!-- this line has an HTML comment but will be rendered and should be translatable. -->\n";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(FilterTestDriver.getEvents(filter, snippet, null));
            assertTUListContains(tus, "this");
            assertTUListContains(tus, "translatable");
        }
    }

    @Test
    public void testImage() {
        String snippet = "Here is an ![Image](https://www.google.com)\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 5, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            assertEquals(EventType.TEXT_UNIT, events.get(1).getEventType());
            assertEquals("Here is an " + IPH0 + "Image" + IPH1,
                         events.get(1).getTextUnit().getSource().getCodedText());
            assertEquals(EventType.DOCUMENT_PART, events.get(2).getEventType());
            assertEquals(EventType.DOCUMENT_PART, events.get(3).getEventType());
            assertEquals(EventType.END_DOCUMENT, events.get(4).getEventType());
        }
    }

    @Test
    public void testExtractImageTitleAndAltText() {
        String snippet = "![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png"
                         + " \"Logo Title Text 1\")";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(FilterTestDriver.getEvents(filter, snippet, null));
            assertEquals(1, tus.size());
            assertEquals("alt text" + IPH0 + "Logo Title Text 1" + IPH1, tus.get(0).getSource().getCodedText());
        }
    }

    @Test
    public void testExtractImageTitleButNotAltText() {
        String snippet = "![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png"
                         + " \"Logo Title Text 1\")";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            Parameters params = filter.getParameters();
            params.setTranslateImageAltText(false);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(FilterTestDriver.getEvents(filter, snippet, null));
            assertEquals(1, tus.size());
            assertEquals("Logo Title Text 1" + IPH0, tus.get(0).getSource().getCodedText());
        }
    }

    @Test
    public void testImageWithTranslatableUrl() {
        String snippet = "Here is an ![Image](https://www.google.com)\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            filter.getParameters().setTranslateUrls(true);
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertEquals(1, tus.size());
            assertEquals("Here is an " + IPH0 +"Image" + IPH1 + "https://www.google.com" + IPH2,
                         tus.get(0).getSource().getCodedText());
            assertEquals(EventType.DOCUMENT_PART, events.get(2).getEventType());
            assertEquals(EventType.DOCUMENT_PART, events.get(3).getEventType());
            assertEquals(EventType.END_DOCUMENT, events.get(4).getEventType());
        }
    }

    @Test
    public void testImageRef() {
        String snippet = "![Image][A]\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 10, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            for (int i = 1; i < 8; i++) {
                assertEquals(EventType.DOCUMENT_PART, events.get(i).getEventType());
            }
            assertEquals(EventType.END_DOCUMENT, events.get(9).getEventType());
        }
    }

    @Test
    public void testImgTagWithAlt() {
        String snippet = "Our logo looks like this:\n" +
                        "<img src=\"https://www.spartansoftwareinc.com/wp-content/uploads/2016/07/Logo-Text.png\" alt=\"Spartan logo\">\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertTUListContains(tus, "Our logo");
            assertTUListContains(tus, "Spartan logo");
            assertTUListDoesNotContain(tus, "Logo-Text.png");
        }
    }

    @Test
    public void testIndentedCodeBlock() {
        String snippet = "    This is text\n"
                + "    in an indented\n"
                + "    code block\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 4, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());

            assertTextUnit(events.get(1),
                      "    This is text\n"
                    + "    in an indented\n"
                    + "    code block\n",
                    "    ", "\n", "    ", "\n", "    ", "\n");
            assertDocumentPart(events.get(2), "\n");

            assertEquals(EventType.END_DOCUMENT, events.get(3).getEventType());
        }
    }

    @Test
    public void testTabIndentedCodeBlock() {
        String snippet = "\tThis is text\n"
                + "\tin an indented\n"
                + "\tcode block\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 4, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());

            assertTextUnit(events.get(1),
                      "    This is text\n"
                    + "    in an indented\n"
                    + "    code block\n",
                    "    ", "\n", "    ", "\n", "    ", "\n");
            assertDocumentPart(events.get(2), "\n");

            assertEquals(EventType.END_DOCUMENT, events.get(3).getEventType());
        }
    }

    @Test
    public void testLink() {
        String snippet = "This is a [Link](<https://www.google.com>)\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertEquals(1, tus.size());
            assertEquals("This is a " + IPH0 + "Link" + IPH1, tus.get(0).getSource().getCodedText());
        }
    }

    @Test
    public void testLinkWithTranslatableUrl() {
        String snippet = "This is a [Link](<https://www.google.com>)\n\n";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            Parameters params = filter.getParameters();
            params.setTranslateUrls(true);
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertEquals(1, tus.size());
            assertEquals("This is a " + IPH0 + "Link" + IPH1 + "https://www.google.com" + IPH2, tus.get(0).getSource().getCodedText());
        }
    }

    @Test
    public void testLinkRef() {
        String snippet = "[Link][A]\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 10, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            for (int i = 1; i < 8; i++) {
                assertEquals(EventType.DOCUMENT_PART, events.get(i).getEventType());
            }
            assertEquals(EventType.END_DOCUMENT, events.get(9).getEventType());
        }
    }

    @Test
    public void testReferenceDefinition() {
        String snippet = "[1]: https://www.google.com 'Google'\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 11, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            for (int i = 1; i < 9; i++) {
                assertEquals(EventType.DOCUMENT_PART, events.get(i).getEventType());
            }
            assertEquals(EventType.END_DOCUMENT, events.get(10).getEventType());
        }
    }

    @Test
    public void testEmphasisAndStrong() {
        String snippet = "Some **strong** and *emphasized* text";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 5, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            assertTextUnit(events.get(1), "Some **strong** and *emphasized* text",
                    "**", "**", "*", "*");
            assertDocumentPart(events.get(2), System.lineSeparator());
            assertDocumentPart(events.get(3), System.lineSeparator());
            assertEquals(EventType.END_DOCUMENT, events.get(4).getEventType());        }
    }

    @Test
    public void testStrikethroughSubscript() {
        String snippet = "Some ~~strikethrough~~ and ~subscript~ text";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 5, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            assertTextUnit(events.get(1), "Some ~~strikethrough~~ and ~subscript~ text",
                    "~~", "~~", "~", "~");
            assertDocumentPart(events.get(2), System.lineSeparator());
            assertDocumentPart(events.get(3), System.lineSeparator());
            assertEquals(EventType.END_DOCUMENT, events.get(4).getEventType());
        }
    }

    @Test
    public void testThematicBreak() {
        String snippet = "---\n\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            assertEquals("#events", 5, events.size());
            assertEquals(EventType.START_DOCUMENT, events.get(0).getEventType());
            assertDocumentPart(events.get(1), "---");
            assertDocumentPart(events.get(2), "\n");
            assertDocumentPart(events.get(3), "\n");
            assertEquals(EventType.END_DOCUMENT, events.get(4).getEventType());
        }
    }

    @Test
    public void testTable1TextUnits() throws Exception {
        RawDocument rd =  new RawDocument(getFileContents("table1_original.md"), LocaleId.ENGLISH);

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, rd);
            assertEquals("#events", 4, events.size());
            assertTextUnit(events.get(0), "Command");
            assertTextUnit(events.get(1), "Description");
            assertTextUnit(events.get(2), "git status`", "`"); // Leading "`" is a document part
            assertTextUnit(events.get(3), "List all **new** or _modified_ files", "**", "**", "_", "_");
        }
    }

    @Test
    public void testTable2TextUnits() throws Exception {
        RawDocument rd =  new RawDocument(getFileContents("table2_original.md"), LocaleId.ENGLISH);

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, rd);
            assertEquals("#events", 10, events.size());
            assertTextUnit(events.get(0), "Left-aligned");
            assertTextUnit(events.get(1), "Center-aligned");
            assertTextUnit(events.get(2), "Right-aligned");
            assertTextUnit(events.get(3), "git status");
            assertTextUnit(events.get(4), "git status");
            assertTextUnit(events.get(5), "git status");
            assertTextUnit(events.get(6), "git diff");
            assertTextUnit(events.get(7), "git diff");
            assertTextUnit(events.get(8), "git diff");
            assertTextUnit(events.get(9), "GitHub](http://github.com)", "](http://github.com)");
        }
    }

    @Test
    public void testDontTranslateFencedCodeBlocks() throws Exception {
        RawDocument rd = new RawDocument(getFileContents("code_and_codeblock_tests.md"), LocaleId.ENGLISH);
        try (MarkdownFilter filter = new MarkdownFilter()) {
            filter.getParameters().setTranslateCodeBlocks(false);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(FilterTestDriver.getTextUnitEvents(filter, rd));
            assertTextUnit(tus.get(0), "Code and Codeblock tests");
            assertTextUnit(tus.get(1), "Code blocks");
            assertTextUnit(tus.get(2), "There are two ways to specify code blocks. One may delimit via four  tildas, like this:");
            assertTextUnit(tus.get(3), "Another is to delimit with three ticks like this:");
            assertTextUnit(tus.get(4), "One may also specify that the code is to be treated with syntax coloring like this:");
            assertTextUnit(tus.get(5), "Inline code blocks");
            assertTextUnit(tus.get(6), "Inline code contain things like `variable names` that we may want to protect.", "`variable names`");
        }
    }

    @Test
    public void testTranslateFencedCodeBlocks() throws Exception {
        RawDocument rd = new RawDocument(getFileContents("code_and_codeblock_tests.md"), LocaleId.ENGLISH);
        try (MarkdownFilter filter = new MarkdownFilter()) {
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(FilterTestDriver.getTextUnitEvents(filter, rd));
            assertTextUnit(tus.get(0), "Code and Codeblock tests");
            assertTextUnit(tus.get(1), "Code blocks");
            assertTextUnit(tus.get(2), "There are two ways to specify code blocks. One may delimit via four  tildas, like this:");
            assertTextUnit(tus.get(3), "This text is within a code block and should remain in English.");
            assertTextUnit(tus.get(4), "Another is to delimit with three ticks like this:");
            assertTextUnit(tus.get(5), "This text is within a code block and should remain in English.");
            assertTextUnit(tus.get(6), "One may also specify that the code is to be treated with syntax coloring like this:");
            assertTextUnit(tus.get(7), "This text is within a code block and should remain in English.");
            assertTextUnit(tus.get(8), "Inline code blocks");
            assertTextUnit(tus.get(9), "Inline code contain things like `variable names` that we may want to protect.", "`", "`");
        }
    }

    @Test
    public void testDontTranslateMetadataHeader() throws Exception {
        RawDocument rd = new RawDocument(getFileContents("metadata_header.md"), LocaleId.ENGLISH);
        try (MarkdownFilter filter = new MarkdownFilter()) {
            // This is the default behavior
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(FilterTestDriver.getTextUnitEvents(filter, rd));
            assertTextUnit(tus.get(0), "This should be the only translatable segment.");
        }
    }

    @Test
    public void testTranslateMetadataHeader() throws Exception {
        RawDocument rd = new RawDocument(getFileContents("metadata_header.md"), LocaleId.ENGLISH);
        try (MarkdownFilter filter = new MarkdownFilter()) {
            filter.getParameters().setTranslateHeaderMetadata(true);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(FilterTestDriver.getTextUnitEvents(filter, rd));
            assertTextUnit(tus.get(0), "value");
            assertTextUnit(tus.get(1), "value1, value2");
            assertTextUnit(tus.get(2), "This should be the only translatable segment.");
        }
    }

    @Test
    public void testATagWithTitleAttr() throws Exception {
        String snippet = "\n<a href=\"google.com\" title=\"title text here\">google it!</a>\n";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertTUListContains(tus, "title text here");
            assertTUListContains(tus, "google it!");
            assertTUListDoesNotContain(tus, "google.com");
        }
    }

    @Test
    public void testATagWithTitletWithinDiv() throws Exception {
        // Flexmark treats <div>...<div> as HTML_BLOCK even when everything is in one line.
        String snippet = "<div><a href=\"google.com\" title=\"title text here\">google it!</a></div>\n";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertTUListContains(tus, "title text here");
            assertTUListContains(tus, "google it!");
            assertTUListDoesNotContain(tus, "google.com");
        }
    }

    @Test
    public void testMathTag() throws Exception {
        // Test if the math block is handled as an untranslatable DocumentPart.
        String mathBlock =
                "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n" +
                "  <mstyle displaystyle=\"true\">\n" +
                "    <msup>\n" +
                "      <mrow>\n" +
                "        <mi> &#x03C1;<!--greek small letter rho--> </mi>\n" +
                "      </mrow>\n" +
                "      <mrow>\n" +
                "        <mi> charge </mi>\n" +
                "      </mrow>\n" +
                "    </msup>\n" +
                "  </mstyle></math>";
        String snippet = "This contains a math block\n\n" +
                mathBlock +
                "\n\n" +
                "End of the math block";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertTUListContains(tus, "This contains a math block");
            assertTUListDoesNotContain(tus, "charge");
        }
    }

    @Test
    public void testMathElementWithCommentBehavior() throws Exception {
        // Test if the math block is handled as an untranslatable DocumentPart.
        String snippet = "This contains a math block\n\n" +
                        "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n" +
                        "<!-- </math> doesn't confuse the parser -->\nHello,World!\n</math>\n\n";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertTUListContains(tus, "This contains a math block");
            assertTUListDoesNotContain(tus, "confuse");
            assertTUListDoesNotContain(tus, "Hello,World!");
        }
    }

    @Test
    public void testMathBlockOnSingleLine() {
        String mathBlock =
                "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">" +
                "  <mstyle displaystyle=\"true\">" +
                "    <msup>" +
                "      <mrow>" +
                "        <mi> &#x03C1;<!--greek small letter rho--> </mi>" +
                "      </mrow>" +
                "      <mrow>" +
                "        <mi> charge </mi>" +
                "      </mrow>" +
                "    </msup>" +
                "  </mstyle></math>";
        String snippet = "This contains a math block\n\n" +
                mathBlock +
                "\n\n" +
                "End of the math block";

        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertTUListContains(tus, "This contains a math block");
            assertTUListDoesNotContain(tus, "charge");
        }
    }

    @Test
    public void testMathBlocksInListItems() throws Exception {
        String mathBlock1 = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n" +
                "  <mstyle displaystyle=\"true\">\n" +
                "    <mover>\n" +
                "      <mrow>\n" +
                "        <mi> H </mi>\n" +
                "      </mrow>\n" +
                "      <mrow>\n" +
                "        <mo> &#x2192;<!--rightwards arrow--> </mo>\n" +
                "      </mrow>\n" +
                "    </mover>\n" +
                "  </mstyle></math>";
        String mathBlock2 = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n" +
                "  <mstyle displaystyle=\"true\">\n" +
                "    <mover>\n" +
                "      <mrow>\n" +
                "        <mi> J </mi>\n" +
                "      </mrow>\n" +
                "      <mrow>\n" +
                "        <mo> &#x2192;<!--rightwards arrow--> </mo>\n" +
                "      </mrow>\n" +
                "    </mover>\n" +
                "  </mstyle></math>";
        String snippet = "<ul><li>\n" +
                mathBlock1 + " is the magnetic field intensity" +
                "</li><li>" +
                mathBlock2 + " is the conduction current density" +
                "</li></ul>\n\n";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertTUListDoesNotContain(tus, "H");
            assertTUListDoesNotContain(tus, "J");
            assertTUListDoesNotContain(tus, "rightwards arrow");
            assertTUListContains(tus, "the magnetic field intensity");
            assertTUListContains(tus, "the conduction current density");
        }
    }

    @Test
    public void testUnderlinedTextWithinAsterisks() { // Okapi issue #684
        String snippet = "**asterisks OR _underscores_**\n";
        try (MarkdownFilter filter = new MarkdownFilter()) {
            ArrayList<Event> events = FilterTestDriver.getEvents(filter, snippet, null);
            List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
            assertEquals(tus.size(), 1);
            String ct = tus.get(0).getSource().getCodedText();
            assertTrue(ct.contains("asterisks OR"));
            assertTrue(ct.contains("underscores"));
            assertFalse(ct.contains("_" ));
        }
    }

    private String getFileContents(String filename) throws Exception {
        try (InputStream is = MarkdownWriterTest.class.getResourceAsStream(filename);
                Scanner scanner = new Scanner(is)) {
            return scanner.useDelimiter("\\A").next();
        }
    }
}
