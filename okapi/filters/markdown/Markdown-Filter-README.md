# Markdown Filter
## Summary
The mark down filter, filter id okf_md, is to handle Markdown files.
There is no unified Markdown specification. This filter tries to handle
most common Markdown syntax with a slight bias towards GitHub Markdown syntax.

## Features and Limitations
Current version has these features and limitations.

### Attribute Text Extraction
The values assigned to the "alt" and other attributes in `<img>` and other tags will be extracted for translation.
Below table describes the supported tag and attribute combinations.

| tag | attributes |
| --- | ---------- |
| a | accesskey, title |
| applet | alt |
| area | accesskey, area, alt |
| button | accesskey, value |
| img | alt, title |
| input | accesskey, alt, placeholder, title, value \* |
| isindex | prompt |
| label | accesskey |
| legend | accesskey |
| object | standby |
| option | label, value |
| optgroup | label |
| param | value |
| table | summary |
| td | abbr |
| textarea | accesskey |


\*: Extraction may not be supported depending on the type value.

### MathML Support
`<math>` tag and other tags defined in [Mathematical Markup Language](https://www.w3.org/TR/MathML), also known as MathML, can be used in HTML blocks.
Anything written within a math element is considered untranslatable and will not be extracted to the XLIF file.
Like the table tags, text following `</math>` on the same line is treated as a part of the HTML block. This means the whitespace after the end tag is ignored and `*text*` is displayed as it is.

### Empty Line Handling Issue
There may be extra empty lines (the lines consisting of only newlines), 
or fewer empty lines, after the extraction, translation and merge.

### Whitespace Handling Issues
Within an HTML block, white spaces may be removed. Note this does not
change the meaning of the HTML block because whitespaces
are not usually kept except within `<pre>` and `</pre>` per the HTML
standard specification.

### No Code Finder Support
Starting from this version (M36?), the code finder is no longer supported.

